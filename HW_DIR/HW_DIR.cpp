
#include <chrono>
#include <filesystem>
#include <iostream>


using namespace std::filesystem;
using namespace std::chrono_literals;


template <typename T>
std::time_t to_time_t(T tp)
{
	using namespace std::chrono;
	auto sctp = time_point_cast<system_clock::duration>(tp - T::clock::now()
		+ system_clock::now());
	return system_clock::to_time_t(sctp);
}

std::string FileTime (path p)
{
	file_time_type file_time = last_write_time(p);
	std::time_t tt = to_time_t(file_time);
	std::tm* gmt = std::gmtime(&tt);
	std::stringstream buffer;
	buffer << std::put_time(gmt, "%A, %d %B %Y %H:%M");
	std::string formattedFileTime = buffer.str();
	//std::cout << formattedFileTime << '\n';
	return formattedFileTime;

}


void ShowDir(const path& inputPath_)
{
	uintmax_t byte = 0;
	int count = 0;

	for (const auto& entry : directory_iterator(inputPath_))
	{

		if (file_size(entry.path()) != 0)
		{
			std::cout << FileTime(entry.path()) << "\t" << file_size(entry.path()) << "\t" << entry.path().filename() << std::endl;
			count++;
			byte += file_size(entry.path());
		}
		else
		{
			std::cout << FileTime(entry.path()) << "\t" << "<DIR>" << "\t" << entry.path().filename() << std::endl;
		}

		

	}
	std::cout << "\t\t" << count << " File(s)\t" << byte << " bytes" << std::endl;
}



int main()
{
	path inputPath;
	std::cout << "Directory of ";
	std::cin >> inputPath;


	
	inputPath.make_preferred();
	
	ShowDir(inputPath);

}
